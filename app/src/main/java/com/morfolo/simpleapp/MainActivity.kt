package com.morfolo.simpleapp

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ItemRecyclerAdapter.ItemRecyclerListener {

    private lateinit var mAdapter: ItemRecyclerAdapter

    private var itemPosition: Int = 0

    companion object {
        private const val PERMISSION_REQ: Int = 1
        private const val PICK_IMAGE: Int = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecycler()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQ) {
            if (!grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                // Do something
            }
            return
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedImage: Uri? = data?.data

                val filePathColumn: Array<String> = arrayOf(MediaStore.Images.Media.DATA)

                val cursor: Cursor = contentResolver.query(
                        selectedImage, filePathColumn, null, null, null)
                val columnIndex: Int = cursor.getColumnIndexOrThrow(filePathColumn[0])
                cursor.moveToFirst()

                val imagePath: String = cursor.getString(columnIndex)
                cursor.close()

                mAdapter.itemChanged(imagePath, itemPosition)
            }
        }
    }

    override fun onItemClick(position: Int) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        PERMISSION_REQ)
            } else {
                pickImageFromGallery(position)
            }
        } else {
            pickImageFromGallery(position)
        }
    }

    private fun pickImageFromGallery(position: Int) {
        itemPosition = position

        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select picture"), PICK_IMAGE)
    }

    private fun initRecycler() {
        mAdapter = ItemRecyclerAdapter(dataStore(), this, this)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = mAdapter
    }

    private fun dataStore(): MutableList<String> {
        val dataList: MutableList<String> = ArrayList()
        (1..10).mapTo(dataList) { "data$it" }
        return dataList
    }
}
