package com.morfolo.simpleapp

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_list_view.view.*
import java.io.File

/**
 * Created by M on 09/11/2017.
 */
class ItemRecyclerAdapter constructor(
        private val dataList: MutableList<String>,
        private val context: Context,
        private val listener: ItemRecyclerListener
) : RecyclerView.Adapter<ItemRecyclerAdapter.ItemVH>() {

    interface ItemRecyclerListener {
        fun onItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemVH {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_list_view, parent, false)
        return ItemVH(view)
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: ItemVH?, position: Int) {
        val item: String = dataList[position]

        Glide.with(context)
                .load(File(item))
                .into(holder?.imageView)

        holder?.itemView?.setOnClickListener { listener.onItemClick(holder.adapterPosition) }
    }

    fun itemChanged(item: String, position: Int) {
        dataList[position] = item
        notifyItemChanged(position)
    }

    class ItemVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.imageView
    }
}