package com.morfolo.simpleapp

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * Created by M on 09/11/2017.
 */
class RectangleLayout(context: Context?, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    private val widthRatio: Double = 2.0
    private val heightRatio: Double = 2.0

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthSize: Int = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize: Int = (heightRatio / widthRatio * widthSize).toInt()
        val newHeightSize: Int = MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY)
        super.onMeasure(widthMeasureSpec, newHeightSize)
    }
}